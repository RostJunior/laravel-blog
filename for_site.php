composer global require "laravel/installer"  						//  установка композера
composer create-project --prefer-dist laravel/laravel Name  		//  установка ларавел
php artisan app:name Corp  											//  если нужно поменять неймспасе с App
php artisan make:migration CreateArticlesTable --create=articles	//  Миграции articles
......																//  ....
php artisan migrate													//  Применить миграции
php artisan make:migration ChangeArticlesTable --table=articles		//  редактируем таблицы (добавляем внешние ключи)

php artisan make:controller IndexController --resource				//	создать контроллер типа ресурс (с методами)
php artisan route:list												//  список маршрутов

.......

наполняем шаблон + контроллер с методами (Site + Index Controllers)
{{asset(env('THEME'))}}  /css/.....									// Папка с темой (для подключения файлов 
php artisan make:model Models\ContentTypes\Menu 					// Создаем модель


методы:

str_limit($item->text, 200)  																			  - лимит вывода текста
is_string($item->image) && is_object(json_decode($item->image)) && (json_last_error() == JSON_ERROR_NONE) - проверка на стрку json
$this->$contentRightBar = view(env('THEME').'.indexBar')->with('articles', $articles)->render();          - создать строку с шаблона
{{$article->created_at->format('F d, Y')}}																  - формат дат