<?php

namespace Corp\Models\ContentTypes;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    
    public function  articles() {
        return $this->hasMany('Corp\Models\ContentTypes\Article');
    }
}
