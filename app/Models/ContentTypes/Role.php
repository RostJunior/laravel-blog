<?php

namespace Corp\Models\ContentTypes;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users() {
        return $this->belongsToMany('Corp\Models\User', 'role_user');
    }
    
    public function perms() {
        return $this->belongsToMany('Corp\Models\ContentTypes\Permission', 'permission_role');
    }
}
