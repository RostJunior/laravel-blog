<?php

namespace Corp\Models\ContentTypes;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
   protected $table = 'menu';
}
