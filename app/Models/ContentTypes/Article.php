<?php

namespace Corp\Models\ContentTypes;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
   protected $table = 'articles';
   
   public function user() {
       return $this->belongsTo('Corp\User');
   }
   
   public function category() {
       return $this->belongsTo('Corp\Models\ContentTypes\Category');
   }
   
   public function comments() {
       return $this->hasMany('Corp\Models\ContentTypes\Comment');
   }
}