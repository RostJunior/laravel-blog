<?php

namespace Corp\Models\ContentTypes;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    
    public function roles() {
        return $this->belongsToMany('Corp\Models\ContentTypes\Role', 'permission_role');
    }
}
