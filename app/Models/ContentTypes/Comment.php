<?php

namespace Corp\Models\ContentTypes;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = ['name', 'text', 'site', 'user_id', 'article_id', 'parent_id', 'email'];


    public function article() {
        return $this->belongsTo('Corp\Models\ContentTypes\Article');
    }
    
    public function user() {
         return $this->belongsTo('Corp\User');
    }
}
