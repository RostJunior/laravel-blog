<?php

namespace Corp\Models\ContentTypes;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
   protected $table = 'sliders';
}