<?php

namespace Corp\Models\ContentTypes;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $table = 'portfolios';
    
    public function filter() {
	
		return $this->belongsTo('Corp\Models\ContentTypes\Filter', 'filter_alias', 'alias');
	}
}
