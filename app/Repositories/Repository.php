<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Corp\Repositories;
use Config;
/**
 * Description of Repository
 *
 * @author Rost
 */
abstract class Repository {
   
    protected $model = false;
    
    
    public function get($select = '*', $take = FALSE, $pagination = false, $where = false) {
       
        $bulder = $this->model->select($select);
        
        if($take) {
        	
            $bulder->take($take);
		}
        if($where) {
            $bulder->where($where[0], $where[1]);
        }        
        if($pagination) {
            return $this->check($bulder->paginate(config('settings.paginate')));
        }
        
        
        
        return $this->check($bulder->get());
    }
    
    protected function check($result) {
        if($result->isEmpty()) {
            return false;
        }
        
        $result->transform(function($item, $key) {
            if(is_string($item->image) && is_object(json_decode($item->image)) && (json_last_error() == JSON_ERROR_NONE)) {
            $item->image = json_decode($item->image);
            }
            return $item;
        });
        
        return $result;
    }
    
    public function one($alias, $attr = []) {
        $result = $this->model->where('alias', $alias)->first();
        return $result;
    }
}
