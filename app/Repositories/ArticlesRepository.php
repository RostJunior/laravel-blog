<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Corp\Repositories;
use Corp\Repositories\Repository;
use Corp\Models\ContentTypes\Article;
/**
 * Description of ArticlesRepository
 *
 * @author Rost
 */
class ArticlesRepository extends Repository {
    
    public function __construct(Article $articles) {
        $this->model = $articles;
    }
    
    public function one($alias, $attr = []) {
        $article = parent::one($alias, $attr);
        if($article && !empty($attr)) {
            $article->load('comments');
            $article->comments->load('user');
        }
        
        return $article; 
    }
    
}
