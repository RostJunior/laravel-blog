<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Corp\Repositories;

use Corp\Models\ContentTypes\Portfolio;
use Corp\Repositories\Repository;
/**
 * Description of MenusRepository
 *
 * @author Rost
 */
class PortfolioRepository extends Repository {
    
    public function __construct(Portfolio $portfolio) {
        $this->model = $portfolio;
    }
    
    public function one($alias, $attr = []) {
        $portfolio = parent::one($alias);
         if($portfolio && $portfolio->image) {
            $portfolio->image = json_decode($portfolio->image);
        }
        
        return $portfolio;
    }
    
    
}