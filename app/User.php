<?php

namespace Corp;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function articles() {
        
        return $this->hasMany('Corp\Models\ContentTypes\Article');
    }
    
    public function comments() {
        return $this->hasMany('Corp\Models\ContentTypes\Comment');
    }
    
    public function roles() {
        return $this->belongsToMany('Corp\Models\ContentTypes\Role', 'role_user');
    }
    
    public function canDo($permission, $require = FALSE) {
        if(is_array($permission)) {
            foreach($permission as $permName) {
                $perName = $this->canDo($permName);
                if($perName && !$require) {
                    return true;
                }else if(!$perName && $require) {
                    return false;
                }
                
                return $require;
            }
         }else{
            
            foreach ($this->roles as $role) {
                foreach($role->perms as $perm) {
                    if(str_is($permission, $perm->name)) {
                        return true;
                    }
                }
            }
        }
        
    }
    
    public function hasRole($name, $require = FALSE) {
        if(is_array($name)) {
            foreach($name as $roleName) {
                $hasRole = $this->canDo($roleName);
                if($hasRole && !$require) {
                    return true;
                }else if(!$hasRole && $require) {
                    return false;
                }
                
                return $require;
            }
         }else{
            
            foreach ($this->roles as $role) {
                foreach($role->name as $name) {
                    if($role->name == $name) {
                        return true;
                    }
                }
            }
            return false;
        }
        
    }
}
