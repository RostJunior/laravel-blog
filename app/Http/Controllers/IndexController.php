<?php

namespace Corp\Http\Controllers;

use Illuminate\Http\Request;
use Corp\Repositories\MenusRepository;
use Corp\Models\ContentTypes\Menu;
use Corp\Repositories\SliderRepository;
use Corp\Repositories\PortfolioRepository;
use Corp\Repositories\ArticlesRepository;

class IndexController extends SiteController
{
    
    public function __construct(SliderRepository $s_rep, PortfolioRepository $p_rep, ArticlesRepository $a_rep) {
        parent::__construct(new MenusRepository(new Menu) );
        $this->a_rep    = $a_rep;
        $this->p_rep    = $p_rep;
        $this->s_rep    = $s_rep;
        $this->bar      = 'right';
        $this->template = env('THEME').'.index';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$portfolios  = $this->getPortfolio();
    	$sliderItems = $this->getSliders();
    	
    	
    	$sliders    = view(env('THEME').'.slider')->with('sliders', $sliderItems)->render();
    	
    	$content    = view(env('THEME').'.content')->with('portfolios', $portfolios)->render();
    	
    	$this->vars = array_add($this->vars, 'content', $content);
        $this->vars = array_add($this->vars, 'sliders', $sliders);
        
        $articles = $this->getArticles();
        
        $this->contentRightBar = view(env('THEME').'.indexBar')->with('articles', $articles)->render();
        
        $this->keywords = 'Home Page';
        $this->meta_desc = 'Home Page';
        $this->title = 'Home Page';
        
        return $this->renderOutput();
    }
    
    public function getSliders() {
	
		$sliders = $this->s_rep->get();
		
		if($sliders->isEmpty()) {
		
			return FALSE;
		}
		
		$sliders->transform(function($item, $key){
			
			$item->image = config('settings.slider_path').'/'.$item->image;
			return $item;
		});
		
		
		return $sliders;
	}
	
	public function getPortfolio(){
		
		$portfolio = $this->p_rep->get('*', config('settings.home_port_count'));
		
		return $portfolio;
	}
        
        protected function getArticles() {
            $articles = $this->a_rep->get(['title', 'created_at', 'image', 'alias'], config('settings.home_articles_count'));
                return $articles;
            }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
