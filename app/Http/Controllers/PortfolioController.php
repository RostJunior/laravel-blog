<?php

namespace Corp\Http\Controllers;


use Corp\Http\Controllers\SiteController;
use Corp\Repositories\PortfolioRepository;
use Corp\Repositories\MenusRepository;
use Corp\Models\ContentTypes\Menu;

class PortfolioController extends SiteController
{
   public function __construct(PortfolioRepository $p_rep) {
        parent::__construct(new MenusRepository(new Menu) );
        
        $this->p_rep    = $p_rep;
      
        $this->template = env('THEME').'.portfolios';
    }
    
    public function index()
    {
        $this->title = 'Portfolio';
        $this->meta_desc = 'Meta desc';
        $this->keywords = 'Keywords';
        
        $portfolios = $this->getPortfolios();
        
       
        $content = view(env('THEME').'.portfolios_content')->with('portfolios', $portfolios)->render();
        $this->vars = array_add($this->vars, 'content', $content);
        
        
        return $this->renderOutput();
    }
    
    public function getPortfolios($take = false, $paginate = true) {
        $portfolios = $this->p_rep->get('*', $take, $paginate);
        
        if($portfolios) {
            $portfolios->load('filter');
        }
        
        return $portfolios;
    }
    
    public function show($alias) {
        
        $portfolio = $this->p_rep->one($alias, $attr = []);
        
        $this->title = $portfolio->title;
        $this->keywords = $portfolio->keywords;
        $this->meta_desc = $portfolio->meta_desc;
        
        $portfolios =  $this->getPortfolios(config('settings.other_portfolios'), false);
        
        $content = view(env('THEME').'.portfolio_content')->with(['portfolio' => $portfolio, 'portfolios' => $portfolios])->render();
        $this->vars = array_add($this->vars, 'content', $content);
    
        return $this->renderOutput();
        
    }
    
}
