<?php

namespace Corp\Http\Controllers;

use Corp\Http\Controllers\SiteController;
use Illuminate\Http\Request;
use Corp\Models\ContentTypes\Menu;
use Corp\Repositories\PortfolioRepository;
use Corp\Repositories\ArticlesRepository;
use Corp\Repositories\MenusRepository;
use Corp\Repositories\CommentsRepository;
use Corp\Models\ContentTypes\Category;
class ArticlesController extends SiteController
{
    public function __construct(PortfolioRepository $p_rep, ArticlesRepository $a_rep, CommentsRepository $c_rep) {
        parent::__construct(new MenusRepository(new Menu) );
        $this->a_rep    = $a_rep;
        $this->p_rep    = $p_rep;
        $this->c_rep    = $c_rep;
        $this->bar      = 'right';
        $this->template = env('THEME').'.articles';
    }
    
      public function index($cat_alias = false)
    {
    	$articles = $this->getArticles($cat_alias);
         $content = view(env('THEME').'.articles_content')->with('articles', $articles)->render();
        $this->vars = array_add($this->vars, 'content', $content);
        
        $comments = $this->getComments(config('settings.recent_comments'));
        $portfolios =  $this->getPortfolios(config('settings.recent_portfolios'));
        $this->contentRightBar = view(env('THEME').'.articlesBar')->with(['comments' => $comments, 'portfolios' => $portfolios]);
        
        return $this->renderOutput();
    }
    
    public function getArticles($alias = false) {
        
        $where = false;
        if($alias) {
            $id = Category::select('id')->where('alias', $alias)->first()->id;
            $where = ['category_id', $id];
        }
        $articles = $this->a_rep->get(['id','title', 'alias', 'created_at', 
            'image', 'desc', 'user_id', 'category_id', 'keywords', 'meta_desc'], false, true, $where);
        
        if($articles) {
           $articles->load('user', 'category', 'comments');
        }
        
        
        return $articles;
    }
    
    public function getComments($take) {
        $comments = $this->c_rep->get(['text', 'name', 'email', 'article_id', 'user_id'], $take);
           if($comments) {
           $comments->load('article', 'user');
        } 
       return $comments; 
    }
    
     public function getPortfolios($take) {
        $portfolios = $this->p_rep->get(['title', 'text', 'alias', 'customer', 'image', 'filter_alias'], $take);
        
        return $portfolios;
                
    }
    
    public function show($alias = false) {
        
        $article = $this->a_rep->one($alias, ['comments' => true]);
        if($article) {
            $article->image = json_decode($article->image);
        }
        
        $this->title = $article->title;
        $this->keywords = $article->keywords;
        $this->meta_desc = $article->meta_desc;
        
        
     //   dd($article->comments->groupBy('parent_id'));
        $content = view(env('THEME').'.article_content')->with('article', $article)->render();
        $this->vars = array_add($this->vars, 'content', $content);
        $comments = $this->getComments(config('settings.recent_comments'));
        
        $portfolios =  $this->getPortfolios(config('settings.recent_portfolios'));
        $this->contentRightBar = view(env('THEME').'.articlesBar')->with(['comments' => $comments, 'portfolios' => $portfolios]);
        
        return $this->renderOutput();
        
    }
}
