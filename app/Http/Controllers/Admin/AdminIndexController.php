<?php

namespace Corp\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Corp\Http\Controllers\Admin\AdminController;
use Gate;
use Auth;
class AdminIndexController extends AdminController
{
    
    public function __construct() {
        parent::__construct();
       
        
        $this->template = 'admin_index';
    }
    
    public function index() {
        if(Gate::denies('VIEW_ADMIN')) {
			abort(404);
		}
         
        $this->title = 'Панель администратора';
     
       return $this->renderOutput();
    }
}
