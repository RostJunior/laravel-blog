<?php

namespace Corp\Http\Controllers;

use Corp\Repositories\MenusRepository;
use Illuminate\Http\Request;
use Menu;

class SiteController extends Controller
{
    protected $p_rep;                 // репозиторий для портфолио
    protected $s_rep;                 // для слайдера
    protected $a_rep;                 // артиклес репозиторий
    protected $m_rep;                 // для меню
    protected $template;              // имя шаблона для отображения информации на странице
    protected $vars = [];             // переменные в шаблон 
    protected $bar = 'no';              // свойство для сайтбара (левый/правый/нету)
    protected $varsleftbar = false;   // переменные для левого сайтбара
    protected $varsrightbar = false;  // переменныя для правого сайтбара
    protected $contentRightBar = false;
    protected $contentLeftBar  = false;
    protected $keywords;
    protected $meta_desc;
    protected $title;

    public function __construct(MenusRepository $m_rep) {
       
        $this->m_rep =  $m_rep;
    }
    
    protected function renderOutput() {
        
        $menu = $this->getMenu();
        
        $navigation = view(env('THEME').'.navigation')->with('menu', $menu)->render(); 
        $this->vars = array_add($this->vars, 'navigation', $navigation);
        
        if($this->contentRightBar) {
            $rightBar = view(env('THEME').'.rightbar')->with('content_rightBar', $this->contentRightBar)->render();
            $this->vars = array_add($this->vars, 'rightBar', $rightBar);
           
        }
        
        if($this->contentLeftBar) {
            $leftBar = view(env('THEME').'.leftbar')->with('content_leftBar', $this->contentLeftBar)->render();
            $this->vars = array_add($this->vars, 'leftBar', $leftBar);
           
        }
         $this->vars = array_add($this->vars, 'bar', $this->bar);
        $footer = view(env('THEME').'.footer')->render();
        $this->vars = array_add($this->vars, 'footer', $footer);
        
        
        $this->vars = array_add($this->vars, 'keywords', $this->keywords);
        $this->vars = array_add($this->vars, 'meta_desc', $this->meta_desc);
        $this->vars = array_add($this->vars, 'title', $this->title); 
        
        
        return view($this->template)->with($this->vars);
    }
     
    public function getMenu() {
        $menu = $this->m_rep->get();
        
        $mBuilder = Menu::make('MyNav', function ($m) use ($menu) {
            foreach ($menu as $item) {
                if($item->parent == 0)  {
                    $m->add($item->title, $item->path)->id($item->id);
                }else {
                    
                    if($m->find($item->parent)) {
                        $m->find($item->parent)->add($item->title, $item->path)->id($item->id); 
                       
                    }
                }
            }
        });
//dd($mBuilder);
        return $mBuilder;
    }
    
}
